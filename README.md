# Leaderboard API

## Instructions:

1. Open Visual Studio as Administrador (Not Required, but useful with some permissions cases)
2. Build Solution

Note:
```
If you modify something in the controllers, areas, etc... 
you need to rebuild the project or the entire solution
```

## Dummy Endpoint:

To test with a dummy endpoint use a tool like (Postman) and make an POST request to the following path:
http://localhost:49782/api/Users

With the following json in the body of the request:
```
{
    "username": "hmuir",
    "password": "Pernix"
}
```

It should return `true`.

![alt post](https://docs.google.com/a/pernix-solutions.com/uc?id=0B_H7DLOQBpYIbXhYLVlfaG5xbGs)

**Note**:
The port may change depending on your local configuration.

## Endpoints documentation
You can find the documentation of the endpoints in the following path:
http://localhost:49782/Help

**Note**:
The port may change depending on your local configuration.

## Deployment

[reference here](deployment.md)
