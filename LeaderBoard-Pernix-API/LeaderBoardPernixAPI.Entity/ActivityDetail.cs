﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LeaderBoard_Pernix_API.DAL;

namespace LeaderBoardPernixAPI.Entity
{
    public class ActivityDetail
    {
        public int idActivity { get; set; }
        public string name { get; set; }
        public int points { get; set; }
        public string frequency { get; set; }
        public string startDate { get; set; }
        public string description { get; set; }
        public string inCharge { get; set; }
    }
}
