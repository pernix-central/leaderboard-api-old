﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaderBoard_Pernix_API.Entity
{
    public class ConfigUser
    {
        public string name { get; set; }
        public string lastName { get; set; }
        public int idRole { get; set; }
    }
}
