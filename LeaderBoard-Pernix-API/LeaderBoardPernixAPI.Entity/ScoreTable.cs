﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LeaderBoard_Pernix_API.DAL;

namespace LeaderBoardPernixAPI.Entity
{
    public class ScoreTable
    {
        public int id { get; set; }
        public string name { get; set; }
        public string lastName { get; set; }
        public int totalPoints { get; set; }
        public string photoUrl { get; set; }
        public string jobPosition { get; set; }
        public int FK_idUser { get; set; }
        public int FK_idJobPosition { get; set; }
        public List<activity> activities { get; set; }
    }
}
