﻿using LeaderBoard_Pernix_API.DAL;
using LeaderBoard_Pernix_API.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Net.Http;
using System.Net;

namespace LeaderBoard_Pernix_API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SessionController : ApiController
    {
        private UserAccessLogic userAccessLogic;

        public SessionController()
        {
            userAccessLogic = new UserAccessLogic();
        }

        // POST: api/session/login/:username&:password
        [HttpPost]
        public IHttpActionResult Login([FromBody]UserAccess userLogin)
        {
            string tokenCode = null;
            try
            {
                tokenCode = userAccessLogic.areCredentialsAvailable(userLogin);
                return Ok(tokenCode);
            }
            catch (unauthorizedException e)
            {
                return Content(HttpStatusCode.Unauthorized, e.Message);
            }
            catch (databaseException e)
            {
                return Content(HttpStatusCode.ServiceUnavailable, e.Message);
            }
            
        }

        // POST: api/session/authorization/:token
        [HttpPost]
        public IHttpActionResult Authorization([FromBody]Token token)
        {
            if (userAccessLogic.validateToken(token.code))
            {
                return Ok(true);
            }
            else
            {
                return Content(HttpStatusCode.Unauthorized, "User unauthorized or password and user are not correct");
            }
        }
    }
}