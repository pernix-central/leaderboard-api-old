﻿using LeaderBoard_Pernix_API.DAL;
using LeaderBoardPernixAPI.Entity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Cors;

namespace LeaderBoard_Pernix_API.Controllers
{
    [EnableCors("*", "*", "*")]
    public class EmployeesController : ApiController
    {
        [HttpGet]
        public IHttpActionResult getEmployees()
        {
            try
            {
                List<ScoreTable> scoreTableList = new List<ScoreTable>();
                Constant.leaderboarDB.members
                    .ToList()
                    .ForEach(m =>
                    {
                        List<jobposition> job = Constant.leaderboarDB.jobpositions.Where(j => j.idJobPosition == m.FK_idJobPosition)
                            .ToList();
                        if (job != null)
                            job.ForEach(j =>
                            {
                                period period = Constant.leaderboarDB.periods
                                    .Where(p => p.startDate <= DateTime.Today
                                                    .Date && p.endDate >= DateTime
                                                    .Today.Date).ToList().FirstOrDefault();
                                historicalscore historical = Constant.leaderboarDB.historicalscores
                                    .Where(hs => hs.FK_idMember == m.idMember &&
                                                 hs.FK_idPeriod == period.idPeriod)
                                    .ToList().FirstOrDefault();
                                if (historical != null)
                                    scoreTableList.Add(new ScoreTable
                                    {
                                        id = m.idMember,
                                        name = m.name,
                                        lastName = m.lastName,
                                        photoUrl = m.photoUrl,
                                        totalPoints = historical.accumulatedPoints.Value,
                                        jobPosition = j.name,
                                        FK_idUser = m.FK_idUser,
                                        FK_idJobPosition = m.FK_idJobPosition,
                                        activities = getActivityByPeriod(period, m)
                                    });
                                else
                                    scoreTableList.Add(new ScoreTable
                                    {
                                        id = m.idMember,
                                        name = m.name,
                                        lastName = m.lastName,
                                        photoUrl = m.photoUrl,
                                        totalPoints = 0,
                                        jobPosition = j.name,
                                        FK_idUser = m.FK_idUser,
                                        FK_idJobPosition = m.FK_idJobPosition,
                                        activities = null
                                    });
                            });
                    });
                var result = scoreTableList.OrderByDescending(element => element.totalPoints);
                return Json(result);
            }
            catch (Exception e)
            {
                return Json(e.Data);
            }
        }

        private List<activity> getActivityByPeriod(period period, member employee)
        {
            List<activity> activitiesList = new List<activity>();
            var participations = Constant.leaderboarDB.participations
                .Where(filter => filter.FK_idMember == employee.idMember &&
                                 filter.participationDate >= period.startDate &&
                                 filter.participationDate <= period.endDate)
                .GroupBy(curParticipation => new {curParticipation.FK_idActivity, curParticipation.FK_idMember})
                .Select(temParticipations => new
                {
                    idActivity = temParticipations.Key.FK_idActivity,
                    points = temParticipations.Select(curParticipation => new {curParticipation.activity.points})
                        .Sum(total => total.points)
                }).ToList();

            participations.ForEach(p =>
            {
                Constant.leaderboarDB.activities.Where(a => a.idActivity == p.idActivity)
                    .ToList()
                    .ForEach(ac =>
                    {
                        activitiesList.Add(new activity
                        {
                            name = ac.name,
                            points = p.points
                        });
                    });
            });
            return activitiesList;
        }

        [HttpDelete]
        public IHttpActionResult deleteMember(int id)
        {
            try
            {
                member lMember = Constant.leaderboarDB.members
                    .First(currentMember => currentMember.idMember == id);
                int lIdUser = lMember.FK_idUser;
                int lIdMember = lMember.idMember;
                deleteParticipationByMember(lIdMember);
                deleteActivity(lIdMember);
                deleteHistoricalScore(lIdMember);
                Constant.leaderboarDB.members.Attach(lMember);
                Constant.leaderboarDB.members.Remove(lMember);
                Constant.leaderboarDB.SaveChanges();
                deleteUser(lIdUser);
                return Ok();
            }
            catch (Exception e)
            {
                return Content(HttpStatusCode.Unauthorized, "Not a Member");
            }
            ;
        }

        public void deleteUser(int id)
        {
            try
            {
                user lUser = Constant.leaderboarDB.users
                    .First(currentUser => currentUser.idUser == id);
                Constant.leaderboarDB.users.Attach(lUser);
                Constant.leaderboarDB.users.Remove(lUser);
                Constant.leaderboarDB.SaveChanges();
            }
            catch (Exception e)
            {
                Trace.TraceError(e.Message);
            }
            ;
        }

        private void deleteActivity(int id)
        {
            try
            {
                List<activity> lActivity = Constant.leaderboarDB.activities
                    .Where(currentActivity => currentActivity.FK_idInCharge == id)
                    .ToList();
                foreach (var lActivityFor in lActivity)
                {
                    int lActityId = lActivityFor.idActivity;
                    deleteParticipationByActivy(lActityId);
                }
                Constant.leaderboarDB.activities
                    .RemoveRange(Constant.leaderboarDB.activities
                        .Where(currentActivity => currentActivity.FK_idInCharge == id));
                Constant.leaderboarDB.SaveChanges();
            }
            catch (Exception e)
            {
                Trace.TraceError(e.Message);
            }
        }

        private void deleteParticipationByMember(int id)
        {
            try
            {
                Constant.leaderboarDB.participations
                    .RemoveRange(Constant.leaderboarDB.participations
                        .Where(currentParticipation => currentParticipation.FK_idMember == id));
                Constant.leaderboarDB.SaveChanges();
            }
            catch (Exception e)
            {
                Trace.TraceError(e.Message);
            }
        }

        private void deleteParticipationByActivy(int id)
        {
            try
            {
                Constant.leaderboarDB.participations
                    .RemoveRange(Constant.leaderboarDB.participations
                        .Where(currentParticipation => currentParticipation.FK_idActivity == id));
                Constant.leaderboarDB.SaveChanges();
            }
            catch (Exception e)
            {
                Trace.TraceError(e.Message);
            }
        }

        private void deleteHistoricalScore(int id)
        {
            try
            {
                historicalscore lHistorical = Constant.leaderboarDB.historicalscores
                    .First(currentHistorical => currentHistorical.FK_idMember == id);
                Constant.leaderboarDB.historicalscores.Attach(lHistorical);
                Constant.leaderboarDB.historicalscores.Remove(lHistorical);
                Constant.leaderboarDB.SaveChanges();
            }
            catch (Exception e)
            {
                Trace.TraceError(e.Message);
            }
        }

        [HttpPost]
        public IHttpActionResult addMember(member newMember)
        {
            try
            {
                Constant.leaderboarDB.members.Add(newMember);
                Constant.leaderboarDB.SaveChanges();
                return Ok();
            }
            catch (Exception e)
            {
                return Content(HttpStatusCode.Unauthorized, "Cannot add a Member");
            }
        }

        [HttpPut]
        public IHttpActionResult editMember(member updateMember)
        {
            try
            {
                member membertoUpdate = Constant.leaderboarDB.members
                    .First(currentMember => currentMember.idMember == updateMember.idMember);
                Constant.leaderboarDB.Entry(membertoUpdate).CurrentValues.SetValues(updateMember);
                Constant.leaderboarDB.SaveChanges();
                return Ok();
            }
            catch (Exception e)
            {
                return Content(HttpStatusCode.Unauthorized, "Cannot update the member" + e);
            }
        }
    }
}