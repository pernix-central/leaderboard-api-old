﻿using LeaderBoard_Pernix_API.DAL;
using LeaderBoardPernixAPI.Entity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Cors;

namespace LeaderBoard_Pernix_API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ActivitiesController : ApiController
    {
        [HttpGet]
        public IHttpActionResult getActivities() {

            List<ActivityDetail> result = new List<ActivityDetail>();
            Constant.leaderboarDB.activities
                                 .ToList()
                                 .ForEach(activity => {
                result.Add(new ActivityDetail
                {
                    idActivity = activity.idActivity,
                    name = activity.name,
                    points = activity.points,
                    frequency = activity.frequency.name,
                    startDate = activity.startDate.ToString("dd/MM/yyy"),
                    description = activity.detail ,
                    inCharge = activity.member.name + " " + activity.member.lastName
                });
            });

         return Json(result);
        }

        [HttpDelete]
        public IHttpActionResult deleteActivity(int id)
        {
            try
            {
                activity lActivity = Constant.leaderboarDB.activities
                    .First(currentActivity => currentActivity.idActivity == id);
                int lActivityNumber = lActivity.idActivity;
                deleteParticipationByActivy(lActivityNumber);
                Constant.leaderboarDB.activities
                        .RemoveRange(Constant.leaderboarDB.activities
                        .Where(currentActivity => currentActivity.idActivity == id));
                Constant.leaderboarDB.SaveChanges();
                return Ok();
            }
            catch (Exception e)
            {
                return Content(HttpStatusCode.Unauthorized, "Not an Activity");
            };
        }

        private void deleteParticipationByActivy(int id)
        {
            try
            {
                Constant.leaderboarDB.participations
                   .RemoveRange(Constant.leaderboarDB.participations
                   .Where(currentParticipation => currentParticipation.FK_idActivity == id));
                Constant.leaderboarDB.SaveChanges();
            }
            catch (Exception e)
            {
                Trace.TraceError(e.Message);
            }
        }

        [HttpPost]
        public IHttpActionResult addActivity(activity newActivity)
        {
            try
            {
                Constant.leaderboarDB.activities.Add(newActivity);
                Constant.leaderboarDB.SaveChanges();
                return Ok();
            }
            catch (Exception e)
            {
                return Content(HttpStatusCode.Unauthorized, "Not an Activity");
            }
        }

        [HttpGet]
        public IHttpActionResult getFrequencies()
        {
            try
            {
                List<frequency> result = new List<frequency>(); 
                Constant.leaderboarDB.frequencies.ToList().ForEach(currentFrequency => {
                    result.Add(new frequency {
                        idFrequency = currentFrequency.idFrequency,
                        name = currentFrequency.name
                    });
                });
                return Json(result);
            }
            catch(Exception e)
            {
                return Content(HttpStatusCode.Unauthorized,"Cannot load the frequencies" + e);
            }
        }

        [HttpPut]
        public IHttpActionResult editActivity(activity updateActivity) {
            try
            {
                activity activityToUpdate = Constant.leaderboarDB.activities
                    .First(currentActivity=>currentActivity
                    .idActivity == updateActivity.idActivity);
                Constant.leaderboarDB.Entry(activityToUpdate).CurrentValues.SetValues(updateActivity);
                Constant.leaderboarDB.SaveChanges();
                return Ok();
            }
            catch (Exception e)
            {
                return Content(HttpStatusCode.Unauthorized,"Cannot update the activity" + e);
            }

        }
    }
}
