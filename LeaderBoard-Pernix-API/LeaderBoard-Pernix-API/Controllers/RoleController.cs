﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LeaderBoard_Pernix_API.DAL;
using System.Web.Http.Cors;
using System.Data.Entity;
using LeaderBoardPernixAPI.Entity;

namespace LeaderBoard_Pernix_API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RoleController : ApiController
    {
        [HttpGet]
        public IHttpActionResult getRoles() {
            try
            {
                List<role> roles = new List<role>();
                Constant.leaderboarDB.roles.ToList().ForEach(currentRole => {
                    roles.Add(new role {
                        idRole = currentRole.idRole,
                        name = currentRole.name
                    });
                });
                return Json(roles);
            }
            catch (Exception e)
            {
                return Content(HttpStatusCode.Unauthorized,"Cannot Load the roles" + e);
            }
        }
    }
}
