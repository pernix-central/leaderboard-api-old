﻿using LeaderBoard_Pernix_API.DAL;
using LeaderBoardPernixAPI.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Cors;

namespace LeaderBoard_Pernix_API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class JobController : ApiController
    {
        [HttpGet]
        public IHttpActionResult getJobPositions() {
            try
            {
                List<jobposition> jobs = new List<jobposition>();
                Constant.leaderboarDB.jobpositions.ToList().ForEach(currentJob => {
                    jobs.Add(new jobposition {
                        idJobPosition = currentJob.idJobPosition,
                        name = currentJob.name
                    });
                });
                return Json(jobs);
            }
            catch (Exception e)
            {
                return Content(HttpStatusCode.Unauthorized, "An error Ocurred" + e);
            }
        }
    }
}
