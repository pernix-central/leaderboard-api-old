﻿using LeaderBoard_Pernix_API.DAL;
using LeaderBoard_Pernix_API.Entity;
using LeaderBoardPernixAPI.Entity;
using System;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Cors;

namespace LeaderBoard_Pernix_API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserController : ApiController
    {
        [HttpPost]
        public IHttpActionResult addUser(ConfigUser config)
        {
            try
            {
                user newUser = new user();
                newUser.username = configureUsername(config.name, config.lastName);
                newUser.password = Constant.password;
                newUser.FK_idRole = config.idRole;
                Constant.leaderboarDB.users.Add(newUser);
                Constant.leaderboarDB.SaveChanges();
                return Content(HttpStatusCode.Created, newUser);
            }
            catch (Exception e)
            { return Content(HttpStatusCode.Unauthorized, "Can´t create the user" + e);
            }
        }

        private string configureUsername(string name, string lastname)
        {
            string username = "";
            username += name.Substring(0, 1).ToLower();
            username += lastname.ToLower();
            if (usernameExists(username))
            {
                username = configureExistsUsername(username);
            }
            return username;
        }

        private bool usernameExists(string username)
        {
            user sameUserName = Constant.leaderboarDB.users
                .FirstOrDefault(currentUsername => currentUsername.username == username);
            if (sameUserName == null)
                return false;
            return true;
        }

        private string configureExistsUsername(string username)
        {
            string newUsername = username;
            char lastLetter = char.Parse(username.Substring(username.Length - 1, 1));
            if (char.IsNumber(lastLetter))
            {
                newUsername += (lastLetter++);
            }
            else
            {
                newUsername += 1;
            }
            return newUsername;
        }
    }
}
