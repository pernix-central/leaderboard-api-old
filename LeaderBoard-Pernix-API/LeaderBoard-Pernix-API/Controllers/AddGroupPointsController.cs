﻿using LeaderBoard_Pernix_API.DAL;
using LeaderBoardPernixAPI.Entity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Cors;

namespace LeaderBoard_Pernix_API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AddGroupPointsController : ApiController
    {
        [HttpPost]
        public IHttpActionResult addGroupPoints(List<participation> participations)
        {
            try
            {
                participations.ForEach(newParticipation =>
                {
                    activity activity = Constant.leaderboarDB
                    .activities
                    .First(currenteActivity => currenteActivity.idActivity ==
                    newParticipation.FK_idActivity);
                    saveParticipation(newParticipation);
                    historicalscore historicalToUpdate = Constant.leaderboarDB
                        .historicalscores
                        .First(currentHistorical => currentHistorical.FK_idMember ==
                        newParticipation.FK_idMember);
                    int pointsRecorded = historicalToUpdate.accumulatedPoints.GetValueOrDefault();
                    int points = activity.points + pointsRecorded;
                    historicalscore updateHistorical = new historicalscore
                    {
                        idHistoricalScore = historicalToUpdate.idHistoricalScore,
                        FK_idMember = historicalToUpdate.FK_idMember,
                        FK_idPeriod = historicalToUpdate.FK_idPeriod,
                        accumulatedPoints = points
                    };
                    updateHistoricalScore(historicalToUpdate, updateHistorical);
                });
                return Ok();
            }
            catch (Exception e)
            {
                return Content(HttpStatusCode.Unauthorized, "Cannot add the points" + e);
            }
        }

        private void saveParticipation(participation newParticipation)
        {
            try
            {
                Constant.leaderboarDB.participations.Add(newParticipation);
                Constant.leaderboarDB.SaveChanges();
            }
            catch (Exception e)
            {
                Trace.TraceError(e.Message);
            }
        }

        private historicalscore getHistoricalScore(int idMember)
        {
            historicalscore score = Constant.leaderboarDB.historicalscores
                .FirstOrDefault(currentHistorical => currentHistorical.FK_idMember == idMember);
            return score;
        }

        private void updateHistoricalScore(historicalscore historicalToUpdate, historicalscore updateHistorical)
        {
            try
            {
                Constant.leaderboarDB.Entry(historicalToUpdate).CurrentValues.SetValues(updateHistorical);
                Constant.leaderboarDB.SaveChanges();
            }
            catch (Exception e)
            {
                Trace.TraceError(e.Message);
            }
        }
    }
}
