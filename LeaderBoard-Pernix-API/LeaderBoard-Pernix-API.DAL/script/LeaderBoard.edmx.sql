-- Generated from EDMX file: C:\Pernix-Projects\leaderboard-api\LeaderBoard-Pernix-API\LeaderBoard-Pernix-API.DAL\LeaderBoard.edmx
-- Target version: 3.0.0.0

-- --------------------------------------------------



-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- NOTE: if the constraint does not exist, an ignorable error will be reported.
-- --------------------------------------------------


--    ALTER TABLE `activity` DROP CONSTRAINT `fk_Activities_Frequencies1`;

--    ALTER TABLE `activity` DROP CONSTRAINT `fk_Activities_Members1`;

--    ALTER TABLE `authentication` DROP CONSTRAINT `fk_Authentication_User1`;

--    ALTER TABLE `historicalscore` DROP CONSTRAINT `fk_HistoricalParticipations_Period1`;

--    ALTER TABLE `historicalscore` DROP CONSTRAINT `fk_HistoricalScores_Members1`;

--    ALTER TABLE `member` DROP CONSTRAINT `fk_Member_User`;

--    ALTER TABLE `member` DROP CONSTRAINT `fk_Members_JobPositions1`;

--    ALTER TABLE `participation` DROP CONSTRAINT `fk_Participation_Activity1`;

--    ALTER TABLE `participation` DROP CONSTRAINT `fk_Participation_Member1`;

--    ALTER TABLE `role_has_permission` DROP CONSTRAINT `fk_Role_has_Permission_Permission1`;

--    ALTER TABLE `role_has_permission` DROP CONSTRAINT `fk_Role_has_Permission_Role1`;

--    ALTER TABLE `user` DROP CONSTRAINT `fk_User_Role1`;


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------
SET foreign_key_checks = 0;

    DROP TABLE IF EXISTS `activity`;

    DROP TABLE IF EXISTS `authentication`;

    DROP TABLE IF EXISTS `frequency`;

    DROP TABLE IF EXISTS `historicalscore`;

    DROP TABLE IF EXISTS `jobposition`;

    DROP TABLE IF EXISTS `member`;

    DROP TABLE IF EXISTS `participation`;

    DROP TABLE IF EXISTS `period`;

    DROP TABLE IF EXISTS `permission`;

    DROP TABLE IF EXISTS `role`;

    DROP TABLE IF EXISTS `role_has_permission`;

    DROP TABLE IF EXISTS `user`;

SET foreign_key_checks = 1;

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------


CREATE TABLE `activities`(
	`idActivity` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`name` varchar (45) NOT NULL, 
	`points` int NOT NULL, 
	`startDate` datetime NOT NULL, 
	`detail` varchar (45) NOT NULL, 
	`FK_idFrequency` int NOT NULL, 
	`FK_idInCharge` int NOT NULL);

ALTER TABLE `activities` ADD PRIMARY KEY (`idActivity`);





CREATE TABLE `authentications`(
	`idAuthentication` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`token` varchar (200) NOT NULL, 
	`logDate` datetime NOT NULL, 
	`expirationDate` datetime NOT NULL, 
	`FK_idUser` int NOT NULL);

ALTER TABLE `authentications` ADD PRIMARY KEY (`idAuthentication`);





CREATE TABLE `frequencies`(
	`idFrequency` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`name` varchar (45) NOT NULL);

ALTER TABLE `frequencies` ADD PRIMARY KEY (`idFrequency`);





CREATE TABLE `historicalscores`(
	`idHistoricalScore` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`accumulatedPoints` int, 
	`FK_idPeriod` int NOT NULL, 
	`FK_idMember` int NOT NULL);

ALTER TABLE `historicalscores` ADD PRIMARY KEY (`idHistoricalScore`);





CREATE TABLE `jobpositions`(
	`idJobPosition` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`name` varchar (45) NOT NULL);

ALTER TABLE `jobpositions` ADD PRIMARY KEY (`idJobPosition`);





CREATE TABLE `members`(
	`idMember` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`name` varchar (45) NOT NULL, 
	`lastName` varchar (45) NOT NULL, 
	`photoUrl` varchar (300) NOT NULL, 
	`FK_idUser` int NOT NULL, 
	`FK_idJobPosition` int NOT NULL);

ALTER TABLE `members` ADD PRIMARY KEY (`idMember`);





CREATE TABLE `participations`(
	`participationDate` datetime NOT NULL, 
	`FK_idMember` int NOT NULL, 
	`FK_idActivity` int NOT NULL, 
	`idParticipation` int NOT NULL AUTO_INCREMENT UNIQUE);

ALTER TABLE `participations` ADD PRIMARY KEY (`idParticipation`);





CREATE TABLE `periods`(
	`idPeriod` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`startDate` datetime NOT NULL, 
	`endDate` datetime NOT NULL);

ALTER TABLE `periods` ADD PRIMARY KEY (`idPeriod`);





CREATE TABLE `permissions`(
	`idPermission` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`name` varchar (45) NOT NULL);

ALTER TABLE `permissions` ADD PRIMARY KEY (`idPermission`);





CREATE TABLE `roles`(
	`idRole` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`name` varchar (45) NOT NULL);

ALTER TABLE `roles` ADD PRIMARY KEY (`idRole`);





CREATE TABLE `users`(
	`idUser` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`username` varchar (45) NOT NULL, 
	`password` varchar (45) NOT NULL, 
	`FK_idRole` int NOT NULL);

ALTER TABLE `users` ADD PRIMARY KEY (`idUser`);





CREATE TABLE `role_has_permission`(
	`permissions_idPermission` int NOT NULL, 
	`roles_idRole` int NOT NULL);

ALTER TABLE `role_has_permission` ADD PRIMARY KEY (`permissions_idPermission`, `roles_idRole`);







-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------


-- Creating foreign key on `FK_idFrequency` in table 'activities'

ALTER TABLE `activities`
ADD CONSTRAINT `fk_Activities_Frequencies1`
    FOREIGN KEY (`FK_idFrequency`)
    REFERENCES `frequencies`
        (`idFrequency`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'fk_Activities_Frequencies1'

CREATE INDEX `IX_fk_Activities_Frequencies1`
    ON `activities`
    (`FK_idFrequency`);



-- Creating foreign key on `FK_idInCharge` in table 'activities'

ALTER TABLE `activities`
ADD CONSTRAINT `fk_Activities_Members1`
    FOREIGN KEY (`FK_idInCharge`)
    REFERENCES `members`
        (`idMember`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'fk_Activities_Members1'

CREATE INDEX `IX_fk_Activities_Members1`
    ON `activities`
    (`FK_idInCharge`);



-- Creating foreign key on `FK_idActivity` in table 'participations'

ALTER TABLE `participations`
ADD CONSTRAINT `fk_Participation_Activity1`
    FOREIGN KEY (`FK_idActivity`)
    REFERENCES `activities`
        (`idActivity`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'fk_Participation_Activity1'

CREATE INDEX `IX_fk_Participation_Activity1`
    ON `participations`
    (`FK_idActivity`);



-- Creating foreign key on `FK_idUser` in table 'authentications'

ALTER TABLE `authentications`
ADD CONSTRAINT `fk_Authentication_User1`
    FOREIGN KEY (`FK_idUser`)
    REFERENCES `users`
        (`idUser`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'fk_Authentication_User1'

CREATE INDEX `IX_fk_Authentication_User1`
    ON `authentications`
    (`FK_idUser`);



-- Creating foreign key on `FK_idPeriod` in table 'historicalscores'

ALTER TABLE `historicalscores`
ADD CONSTRAINT `fk_HistoricalParticipations_Period1`
    FOREIGN KEY (`FK_idPeriod`)
    REFERENCES `periods`
        (`idPeriod`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'fk_HistoricalParticipations_Period1'

CREATE INDEX `IX_fk_HistoricalParticipations_Period1`
    ON `historicalscores`
    (`FK_idPeriod`);



-- Creating foreign key on `FK_idMember` in table 'historicalscores'

ALTER TABLE `historicalscores`
ADD CONSTRAINT `fk_HistoricalScores_Members1`
    FOREIGN KEY (`FK_idMember`)
    REFERENCES `members`
        (`idMember`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'fk_HistoricalScores_Members1'

CREATE INDEX `IX_fk_HistoricalScores_Members1`
    ON `historicalscores`
    (`FK_idMember`);



-- Creating foreign key on `FK_idJobPosition` in table 'members'

ALTER TABLE `members`
ADD CONSTRAINT `fk_Members_JobPositions1`
    FOREIGN KEY (`FK_idJobPosition`)
    REFERENCES `jobpositions`
        (`idJobPosition`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'fk_Members_JobPositions1'

CREATE INDEX `IX_fk_Members_JobPositions1`
    ON `members`
    (`FK_idJobPosition`);



-- Creating foreign key on `FK_idUser` in table 'members'

ALTER TABLE `members`
ADD CONSTRAINT `fk_Member_User`
    FOREIGN KEY (`FK_idUser`)
    REFERENCES `users`
        (`idUser`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'fk_Member_User'

CREATE INDEX `IX_fk_Member_User`
    ON `members`
    (`FK_idUser`);



-- Creating foreign key on `FK_idMember` in table 'participations'

ALTER TABLE `participations`
ADD CONSTRAINT `fk_Participation_Member1`
    FOREIGN KEY (`FK_idMember`)
    REFERENCES `members`
        (`idMember`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'fk_Participation_Member1'

CREATE INDEX `IX_fk_Participation_Member1`
    ON `participations`
    (`FK_idMember`);



-- Creating foreign key on `FK_idRole` in table 'users'

ALTER TABLE `users`
ADD CONSTRAINT `fk_User_Role1`
    FOREIGN KEY (`FK_idRole`)
    REFERENCES `roles`
        (`idRole`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'fk_User_Role1'

CREATE INDEX `IX_fk_User_Role1`
    ON `users`
    (`FK_idRole`);



-- Creating foreign key on `permissions_idPermission` in table 'role_has_permission'

ALTER TABLE `role_has_permission`
ADD CONSTRAINT `FK_role_has_permission_permission`
    FOREIGN KEY (`permissions_idPermission`)
    REFERENCES `permissions`
        (`idPermission`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;



-- Creating foreign key on `roles_idRole` in table 'role_has_permission'

ALTER TABLE `role_has_permission`
ADD CONSTRAINT `FK_role_has_permission_role`
    FOREIGN KEY (`roles_idRole`)
    REFERENCES `roles`
        (`idRole`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;


-- Creating non-clustered index for FOREIGN KEY 'FK_role_has_permission_role'

CREATE INDEX `IX_FK_role_has_permission_role`
    ON `role_has_permission`
    (`roles_idRole`);



-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------
