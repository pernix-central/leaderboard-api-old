﻿using LeaderBoard_Pernix_API.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaderBoard_Pernix_API.DAL
{
    public class UserAccess
    {
        public string username { get; set; }
        public string password { get; set; }
        public string token { get; set; }
    }
}
