﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace LeaderBoard_Pernix_API.BLL
{
    public class Token
    {
       public string code { get; set;}

        public string createTokenCode()
        {
            byte[] tokenBytes = new byte[64];
            using (var rng = new RNGCryptoServiceProvider())
            {
                rng.GetBytes(tokenBytes);
            }
            return BitConverter.ToString(tokenBytes); ;
        } 
    }
}
