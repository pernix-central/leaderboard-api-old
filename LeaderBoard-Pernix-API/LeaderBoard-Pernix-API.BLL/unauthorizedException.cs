﻿using System;
using System.Runtime.Serialization;

namespace LeaderBoard_Pernix_API.BLL
{
    [Serializable]
    public class unauthorizedException : Exception
    {
        public unauthorizedException()
        {
        }

        public unauthorizedException(string message) : base(message)
        {
        }

        public unauthorizedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected unauthorizedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}