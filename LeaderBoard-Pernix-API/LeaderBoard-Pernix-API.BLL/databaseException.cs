﻿using System;
using System.Runtime.Serialization;

namespace LeaderBoard_Pernix_API.BLL
{
    [Serializable]
    public class databaseException : Exception
    {
        public databaseException()
        {
        }

        public databaseException(string message) : base(message)
        {
        }

        public databaseException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected databaseException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}