﻿using LeaderBoard_Pernix_API.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Remoting.Contexts;
using System.Security.Cryptography;

namespace LeaderBoard_Pernix_API.BLL
{
    public class UserAccessLogic
    {
        private authentication authToken;
        private user userDb;
        private leaderboardEntities database;
        
        public UserAccessLogic()
        {
            database = new leaderboardEntities();
        }

        public bool validateToken(string token)
        {
            try
            {
                authToken = (from a in database.authentications where a.token == token select a).Single();
                int result = DateTime.Compare(DateTime.Now, authToken.expirationDate);
                if (result >= 0)
                {
                    return false;
                }
                else
                {
                    return authToken.token.Equals(token);
                }
            }
            catch (Exception e)
            {
                throw e = new unauthorizedException("Token invalid");
            }
        }

        public string areCredentialsAvailable(UserAccess user)
        {
            Token token = new Token();
            try
            {
                string encryptPassword = Encrypt.EncryptString(user.password, user.password);
                userDb = (from u in database.users where u.username == user.username && u.password == encryptPassword select u).Single();
            }

            catch (Exception e)
            {
                throw e = new unauthorizedException("User unauthorized or password and user are not correct");
            }

            try
            {
                authToken = (from a in database.authentications where a.FK_idUser == userDb.idUser && a.token == user.token select a).Single();
            }
            catch
            {
                authToken = null; 
            }


            if (authToken != null)
            {
                if (validateToken(authToken.token))
                {
                    token.code = authToken.token;
                    return authToken.token;
                }
                else
                {
                    throw new unauthorizedException("Token expired");
                }
            }
            else
            {
                
                authentication authentication = new authentication();

                authentication.token = token.createTokenCode();
                authentication.logDate = DateTime.Now;
                authentication.expirationDate = DateTime.Now.AddDays(7);
                authentication.FK_idUser = userDb.idUser;

                database.authentications.Add(authentication);
                
                try
                {
                    database.SaveChanges();
                }

                catch (Exception e)
                {
                    throw e = new databaseException("Error saving data on database");
                }
                return authentication.token;
            }
        }
    }   
}
